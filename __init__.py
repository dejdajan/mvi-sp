from flask import Flask, render_template, request
from create_song import *
import subprocess
import settings
import pickle
from datetime import datetime
from shutil import copyfile

app = Flask(__name__)

rows = range(1,4)
cols = range(1,4)

data_chain = None
phrases = None
playing = None

@app.route('/')
def index():
	return render_template('template.html', no_cols=rows, no_rows=cols, mute=settings.MUTE_FIRST_PROB, 
		ticks=settings.TICKS_LEN, noinstr=settings.NO_INSTRUMENTS, lspead=settings.EVOLUTION_SPEED)

@app.route('/train')
def train():
	global data_chain, phrases
	data_chain, phrases = create_data(settings.IN_DIR)
	generate()
	return index()

@app.route('/load')
def load():
	global data_chain, phrases
	try:
		with open(settings.PICKLE_DATA, 'rb') as handle:
			data_chain = pickle.load(handle)
		with open(settings.PICKLE_PHRASES, 'rb') as handle:
			phrases = pickle.load(handle)

	except:
		train()

	return index()

@app.route('/generate')
def generate(instrs = None):
	global data_chain, phrases
	if data_chain is None or phrases is None:
		load()
	
	for i in range(0,9):
		create_song(data_chain, phrases, i, instrs)
	
	return index()

@app.route('/shut')
def shut():
	global playing
	if playing is not None:
		playing.terminate()
		playing = None
	return index()


@app.route('/listen/<row>/<col>')
def listen(row, col):
	global playing
	if playing is not None:
		playing.terminate()
		playing = None
	print('listening',row,col)
	ind = (int(row)-1)*3 + int(col)-1
	playing = subprocess.Popen(["timidity", settings.OUT_DIR+str(ind) + ".mid"])
	return index()

@app.route('/use/<row>/<col>')
def use(row, col):
	global data_chain, phrases
	if data_chain is None or phrases is None:
		load()

	ind = (int(row)-1)*3 + int(col)-1

	instrs = evolve(data_chain, phrases, ind)
	generate(instrs = instrs)
	return index()

@app.route('/save/<row>/<col>')
def save(row, col):
	ind = (int(row)-1)*3 + int(col)-1
	d = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
	copyfile(settings.OUT_DIR + str(ind) + '.mid', settings.GOOD_DIR + d + '.mid')
	return index()

@app.route('/set', methods=['POST'])
def my_form_post():
	try:
		settings.MUTE_FIRST_PROB = int(request.form["mute"])
	except:
		pass

	try:
		settings.TICKS_LEN = int(request.form["ticks"])
	except:
		pass

	try:
		settings.NO_INSTRUMENTS = int(request.form["instr"])
	except:
		pass

	try:
		settings.EVOLUTION_SPEED = int(request.form["speed"])
	except:
		pass
		
	return index()

if __name__ == '__main__':
	app.run(debug=True)