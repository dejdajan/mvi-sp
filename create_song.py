import midi
import os
import random
import settings
import pickle
from tqdm import tqdm
from markov import create_chain
from markov import add_to_chain
from markov import MarkovCounter

def create_sequences(track, instrs, data = None, phrases = None):
	instr = None
	pitch_seq = []
	tick_seq = []
	velocity_seq = []
	type_seq = []

	for ev in track:
		if isinstance(ev, midi.events.ProgramChangeEvent):
			instr = ev.data[0]
			instrs.append(instr)

		if isinstance(ev, midi.events.NoteOnEvent) or isinstance(ev, midi.events.NoteOffEvent):
			if instr is not None:
				pitch, velocity = ev.data
				tick = ev.tick

				if data is not None and phrases is not None and instr not in data:
					phrases[instr] = []
					data[instr] = []
				pitch_seq.append(pitch)
				velocity_seq.append(velocity)
				tick_seq.append(tick)
				track_type = "on"

				if isinstance(ev, midi.events.NoteOffEvent) or velocity==0:
					track_type = "off"
				type_seq.append(track_type)

	return instr, pitch_seq, tick_seq, velocity_seq, type_seq


def add_data(path, data=None, phrases=None):

	if data == None:
		data = {}
	if phrases == None:
		phrases = {}

	try:
		mfile = midi.read_midifile(path)
	except:
		return data, phrases

	instrs = []
	for track in mfile:
		instr, pitch_seq, tick_seq, velocity_seq, type_seq = create_sequences(track, instrs, data=data, phrases = phrases)

		if instr in data:
			data[instr].extend(pitch_seq)

			for i in range(0,len(tick_seq), settings.PHRASE_LEN):
				phrase_end = i + settings.PHRASE_LEN
				phrases[instr].append({
				"pitch": pitch_seq[i:phrase_end],
				"velocity": velocity_seq[i:phrase_end],
				"tick": tick_seq[i:phrase_end],
				"type": type_seq[i:phrase_end]
				})
	return data, phrases

def create_data(directory):
	data = {}
	data_chain = {}
	phrases = {}
	print('Loading data from', directory,'...')
	for filename in tqdm(os.listdir(directory)):
		if filename.endswith(".mid"): 
			add_data(os.path.join(directory, filename), data, phrases)

	for instr in data:
		data_chain[instr] = create_chain(data[instr])

	with open(settings.PICKLE_DATA, 'wb') as handle:
		pickle.dump(data_chain, handle, protocol=pickle.HIGHEST_PROTOCOL)
	with open(settings.PICKLE_PHRASES, 'wb') as handle:
		pickle.dump(phrases, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return data_chain, phrases

def build_track(instrument, newTicks, newVelocity, newNotes, newTypes):
	track = midi.Track()
	notesOn = []
	p = midi.ProgramChangeEvent()
	p.set_value(instrument)
	track.append(p)

	for i in range(0,len(newNotes)):
		if newTypes[i] == 'on':
			ev = midi.NoteOnEvent(tick=newTicks[i], velocity=newVelocity[i], pitch=newNotes[i])
			notesOn.append(newNotes[i])

		else:
			ev = midi.NoteOffEvent(tick=newTicks[i], velocity=newVelocity[i], pitch=newNotes[i])
			if(newNotes[i] in notesOn):
				notesOn.remove(newNotes[i])

		track.append(ev)
		if random.uniform(0, 100) < settings.MUTE_FIRST_PROB and len(notesOn) > 0:
			for j in range(0,notesOn.count(notesOn[0])):
				ev = midi.NoteOffEvent(tick=1, pitch=notesOn[0])
				track.append(ev)

			notesOn = list(filter((notesOn[0]).__ne__, notesOn))

	eot = midi.EndOfTrackEvent(tick=1)
	track.append(eot)
	return track

def get_random_phrase_starting(phrases, instr, note):
	good = []
	for phrase in phrases[instr]:
		if phrase['pitch'][0] == note:
			good.append(phrase)

	if len(good) == 0:
		good.append(random.choice(phrases[instr]))

	return random.choice(good)

def select_phrase(phrases, instr, lastPhrase=None, chain_notes=None):
	if lastPhrase is None or chain_notes is None:
		return random.choice(phrases[instr])
	
	lastNote = lastPhrase['pitch'][-1]
	nextNote = chain_notes[lastNote].pick_at_random()
	return get_random_phrase_starting(phrases, instr, nextNote)

def shorten_long_ticks(ticks, max_tick=300):
	newTicks = []
	for t in ticks:
		if t > max_tick:
			newTicks.append(max_tick)
		else:
			newTicks.append(t)
	return newTicks

def create_track(data, phrases, instr, firstNote=None):	
	if instr == -1:
		instr = random.choice(list(data.keys()))
		while len(data[instr].keys()) == 0:
			instr = random.choice(list(data.keys()))

	chain_notes = data[instr]
	newNotes = []
	newTicks = []
	newVelocity = []
	newTypes = []

	curPhrase = select_phrase(phrases, instr)
	if firstNote is not None:
		goodNotes = get_good_notes(firstNote)
		counter = 0
		while curPhrase['pitch'][0] not in goodNotes and counter < 10:
			curPhrase = select_phrase(phrases, instr)
			counter += 1

	while sum(newTicks) < settings.TICKS_LEN:
		newNotes.extend(curPhrase['pitch'])
		newTicks.extend(shorten_long_ticks(curPhrase['tick']))
		newVelocity.extend(curPhrase['velocity'])
		newTypes.extend(curPhrase['type'])

		curPhrase = select_phrase(phrases, instr, curPhrase, chain_notes)

	return newNotes[0], build_track(instr, newTicks, newVelocity, newNotes, newTypes)

def get_good_notes(note):
	good = []
	cur = note
	while(cur >= 30):
		cur -= 12

	while cur <= 108:
		good.append(cur)
		good.append(cur + 5)
		good.append(cur + 7)
		cur += 12

	return good

def create_song(data, phrases, name, instrs = None):
	pattern = midi.Pattern()

	track = midi.Track()
	track.append(midi.TrackNameEvent())
	track.append(midi.TextMetaEvent())
	te = midi.SetTempoEvent()
	te.tick = settings.TICKS_LEN
	te.set_mpqn(settings.MPQN)
	track.append(te)
	track.append(midi.EndOfTrackEvent())
	pattern.append(track)

	if instrs is None:
		firstTone, tr = create_track(data, phrases, -1)
		no_instr = settings.NO_INSTRUMENTS
	else:
		firstTone, tr = create_track(data, phrases, instrs[0])
		no_instr = len(instrs)
	pattern.append(tr)


	for i in range(1, no_instr):
		if instrs is None:
			firstTone, tr = create_track(data, phrases, -1, firstTone)
		else:
			firstTone, tr = create_track(data, phrases, instrs[i], firstTone)
		pattern.append(tr)
	
	midi.write_midifile(settings.OUT_DIR+str(name)+".mid", pattern)

def evolve(data_chain, phrases, ind):
	try:
		mfile = midi.read_midifile(settings.OUT_DIR + str(ind) + ".mid")
	except:
		return data, phrases

	instrs = []
	for track in mfile:
		instr, pitch_seq, tick_seq, velocity_seq, type_seq = create_sequences(track, instrs)

		if instr is not None:
			add_to_chain(data_chain[instr], pitch_seq, settings.EVOLUTION_SPEED)

			for i in range(0,len(tick_seq), settings.PHRASE_LEN):
				phrase_end = i + settings.PHRASE_LEN
				phrases[instr].append({
				"pitch": pitch_seq[i:phrase_end],
				"velocity": velocity_seq[i:phrase_end],
				"tick": tick_seq[i:phrase_end],
				"type": type_seq[i:phrase_end]
				})

	return instrs

