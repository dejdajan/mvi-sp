# MI-MVI - Semestral work

This is an implementation of semestral work for class MI-MVI at FIT CTU. The goal of this task was to create application for MIDI music evolution, which is driven by user preferences.

## Getting Started

To build this app, use these commands:

```
git clone git@gitlab.fit.cvut.cz:dejdajan/mvi-sp.git
cd MI-MVI_semestralka
unzip data.zip
rm data.zip
python3 __init__.py
```

**Don't forget to update settings.py file with your paths and create the directories needed!**

Then open http://127.0.0.1:5000/ in your browser. Simple easy-to-use web application will start.

### Prerequisites

* [midi](https://github.com/louisabraham/python3-midi)

* Flask

* pickle

* tqdm

* timidity (available via apt-get)
