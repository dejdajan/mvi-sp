import random
import re
import collections
import itertools

class MarkovCounter(collections.Counter):
    @property
    def total(self):
        return sum(self.values())

    def probability(self, key):
        return self[key] / self.total

    def probabilities(self):
        total = self.total
        for key, value in self.items():
            yield key, value / total

    def pick_most_common(self):
        (most_common, _), = self.most_common(1)
        return most_common

    def pick_at_random(self):
        i = random.randrange(sum(self.values()))
        return next(itertools.islice(self.elements(), i, None))

def pairwize(iterable):
	first, second = itertools.tee(iterable)
	next(second, None)
	return zip(first, second)

def create_chain(words):
	markov_chain = collections.defaultdict(MarkovCounter)
	for current_word, next_word in pairwize(words):
		markov_chain[current_word][next_word] += 1
	return markov_chain

def add_to_chain(markov_chain, words, speed):
    for current_word, next_word in pairwize(words):
        markov_chain[current_word][next_word] += speed
