MUTE_FIRST_PROB = 5
TICKS_LEN = 5000
MPQN = 20
NO_INSTRUMENTS = 3
PHRASE_LEN = 16
EVOLUTION_SPEED = 2

# Directory for output songs - needs to be created
OUT_DIR = "songs/"

# Directory for songs saved with "Save" button - needs to be created
GOOD_DIR = "goodies/"

#Directory with input midi files
IN_DIR = "data/"

# Serialized .pickle files
PICKLE_DATA = "data.pickle"
PICKLE_PHRASES = "phrases.pickle"
